import React, { useState, useEffect } from "react";
import axios from "axios";
import { Link } from "react-router-dom";

const Home = () => {
  const [users, setUser] = useState([]);

  useEffect(() => {
    loadUsers();
  }, []);

  const loadUsers = async () => {
    const keyword = '';
    const pageNumber=1;
    const result = await axios.get(`http://localhost:5000/api/user/getUserLiting?keyword=${keyword}&pageNumber=${pageNumber}`);
    setUser(result.data.result.response.list.reverse());
  };

  const deleteUser = async user => {
    user.type=0;
    await axios.post("http://localhost:5000/api/user/create",user);
    loadUsers();
  };

  return (
    <div className="container">
      <div className="py-4">
        <h1>USER LIST</h1>
        <table className="table border shadow">
          <thead className="thead-dark">
            <tr>
              <th scope="col">#</th>
              <th scope="col">Name</th>
              <th scope="col">Role</th>
              <th scope="col">Email</th>
              <th scope="col">Status</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            {users.map((user, index) => (
              <tr key={index}>
                <th scope="row">{index + 1}</th>
                <td>{user.first_name} {user.last_name}</td>
                <td>{user.role===0?"Admin":"Partner"}</td>
                <td>{user.email}</td>
                <td>{user.status===1?"Active":"Inactive"}</td>
                <td>
                  <Link
                    className="btn btn-outline-primary mr-2"
                    to={`/users/edit/${user.id}`}
                  >
                    Edit
                  </Link>
                  <Link
                    className="btn btn-danger" to="/#"
                    onClick={() => deleteUser(user)}
                  >
                    Delete
                  </Link>
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    </div>
  );
};

export default Home;
